// Create express app
var express = require("express")
var app = express()
var db = require("./database.js")
var sqlite = require('sqlite-sync'); //подключем шайтанмодуль для синхронных запросов к базе
var SqlString = require('sqlstring');
var md5 = require("md5")
var remote = require("webix-remote");
var api = remote.server();

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// шарим папку
app.use('/desktop', express.static(__dirname + '/desktop'));
app.use('/login', express.static(__dirname + '/login'));

// registering the webix remote function
api.setMethod("auth", function(username,password){
    check = checkUserPass(username, password)
    return check
});

api.setMethod("getUsers", function(username,password){
    table = getTableUsers();
    return table
});
api.setMethod("createUser", function(username,email,phone,password){
    newuser = createNewUser(username, email, phone, password);
    return newuser
});
api.setMethod("deleteUser", function(id){
    deluser = deleteUser(id);
    return deluser
});
api.setMethod("editUser", function(id,username,email,phone,password){
        console.log(id)
    console.log(username)
    console.log(email)
    console.log(phone)
    console.log(password)
    edituser = editUser(id,username,email,phone,password)
    return edituser
});




api.setMethod("add", function(a,b){
    return a+b;
});

app.use("/api", api.express());


function checkUserPass(username, password){
    sqlite.connect('db.sqlite'); 
    var hash = md5(password);
    var sql = SqlString.format('SELECT id, name, email, phone FROM user WHERE name = ? OR email = ? OR phone = ? AND password = ?', [username, username, username, hash]);
    var ret = ''
    sqlite.run(sql, function(res){
        if(res.error){
            throw res.error;
        }
        console.log(res);
        ret = res
    });
    sqlite.close();

    if (ret.length > 0){
    console.log(ret);
    return ret[0]
    }else{
    console.log(ret);
    return {"name":"error"}
    }
}

function getTableUsers(user){
    sqlite.connect('db.sqlite'); 
    var sql = SqlString.format('SELECT * FROM user');
    var ret = ''
    sqlite.run(sql, function(res){
        if(res.error){
            throw res.error;
        }
        console.log(res);
        ret = res
    });
    sqlite.close();

    if (ret.length > 0){
    //console.log(ret);
    return ret
    }else{
    //console.log(ret);
    return {"name":"error"}
    }
}


function createNewUser(username,email,phone,password){
    sqlite.connect('db.sqlite');
    console.log(username)
    console.log(email)
    console.log(phone)
    console.log(password)
    var hash = md5(password)
    var sql = SqlString.format('INSERT INTO user (name, email, phone, password) VALUES (?, ?, ?, ?)', [username, email, phone, hash]);
    console.log(sql)
    var ret = ''
    sqlite.run(sql, function(res){
        if(res.error){
            throw res.error;
        }
        console.log(res);
        ret = res
    });
    sqlite.close();
    return ret
}

function deleteUser(id){
    sqlite.connect('db.sqlite');
    var sql = SqlString.format('DELETE FROM user WHERE id = ?', [id]);
    sqlite.run(sql, function(res){
        if(res.error){
            throw res.error;
        }
        console.log(res);
        ret = res
    });
    sqlite.close();
    return ret
}


function editUser(id,username,email,phone,password){
    sqlite.connect('db.sqlite');
    var hash = md5(password)
    console.log(id)
    console.log(username)
    console.log(email)
    console.log(phone)
    console.log(password)
    var sql = SqlString.format('UPDATE user SET name = ?, email = ?, phone = ?, password = ? WHERE id = ?', [username, email, phone, hash, id]);
    sqlite.run(sql, function(res){
        if(res.error){
            throw res.error;
        }
        console.log(res);
        ret = res
    });
    sqlite.close();
    return ret    
}




// passport.use(new LocalStrategy(function(username, password, done) {
//   db.get('SELECT salt FROM users WHERE username = ?', username, function(err, row) {
//     if (!row) return done(null, false);
//     var hash = hashPassword(password, row.salt);
//     db.get('SELECT username, id FROM users WHERE username = ? AND password = ?', username, hash, function(err, row) {
//       if (!row) return done(null, false);
//       return done(null, row);
//     });
//   });
// }));



// Server port
var HTTP_PORT = 8000 
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// Root endpoint
app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

// Insert here other API endpoints
app.get("/api/users", (req, res, next) => {
    var sql = "select * from user"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});

app.get("/api/user/:id", (req, res, next) => {
    var sql = "select * from user where id = ?"
    var params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":row
        })
      });
});

app.post("/api/user/", (req, res, next) => {
    var errors=[]
    if (!req.body.password){
        errors.push("No password specified");
    }
    if (!req.body.email){
        errors.push("No email specified");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
        name: req.body.name,
        email: req.body.email,
        password : md5(req.body.password)
    }
    var sql ='INSERT INTO user (name, email, password) VALUES (?,?,?)'
    var params =[data.name, data.email, data.password]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

app.patch("/api/user/:id", (req, res, next) => {
    var data = {
        name: req.body.name,
        email: req.body.email,
        password : req.body.password ? md5(req.body.password) : null
    }
    db.run(
        `UPDATE user set 
           name = COALESCE(?,name), 
           email = COALESCE(?,email), 
           password = COALESCE(?,password) 
           WHERE id = ?`,
        [data.name, data.email, data.password, req.params.id],
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
    });
})


app.delete("/api/user/:id", (req, res, next) => {
    db.run(
        'DELETE FROM user WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", changes: this.changes})
    });
})




// Default response for any other request
app.use(function(req, res){
    res.status(404);
});