function checkFields(){
	var username = $$('log_form').getValues().username
	var password = $$('log_form').getValues().password
	alert(username + '|||' + password)
}



webix.ui({
    rows:[
        {template:"Header"},
        {cols:[
            {template:"Left"},
            {
            	view:"form", 
			    id:"log_form",
			    width:400,
			    elements:[
			        { view:"text", label:"Логин", name:"username", tooltip:"Введите ваше имя пользователя"},
			        { view:"text", type:"password", label:"Пароль", name:"password", tooltip:"Введите ваш пароль"},
			        { view:"checkbox", labelRight: "Запомнить", name:"accept"},
			        { margin:5, cols:[
			            { view:"button", width:100, value:"ВХОД" , css:"webix_primary", tooltip:"Войти окей да", click:"checkUser"},
			            { view:"button", value:"Забыли пароль или логин?", tooltip:"Плохо дело, сейчас что-нибудь придумаем"}
			        ]}
			    ]
			},
            {template:"Right"}
        ]},
        {template:"Footer"}
    ]
});

